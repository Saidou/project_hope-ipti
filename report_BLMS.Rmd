---
output: pdf_document
classoption: landscape
papersize: a4
geometry: "left=1cm, right=1cm, top=1.5cm, bottom=1cm"
pagestyle: empty
header-includes:
  - \usepackage{tabto}
  - \usepackage{booktabs}
  - \usepackage{array}
  - \usepackage{float}
  - \usepackage{tabu}
  - \renewcommand{\familydefault}{\sfdefault}
---

\begin{center}
\Large{\textbf{HOOPE-ITPI: Health expenditure description}}
\end{center}

```{r echo = FALSE, warning = FALSE, error = FALSE, message = FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(knitr)
library(kableExtra)
```


```{r }
df_merge1 %>% 
  select(-stat_0_1) %>% 
  kable(booktabs = T, format = "latex", linesep = "",
        caption = "Health expenditure description, 2020 survey, US\\$.",
        col.names = c("Variable",
                      "No (n = 145)", "Yes (n = 196)", "p.value", "Uninsured (n = 155)", "Insured (n = 186)", "p.value",
                      "No (n = 242)", "Yes (n = 122)", "p.value", "Uninsured (n = 228)", "Insured (n = 136)", "p.value"),
        align = c("l", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r", "r"), format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position", "scale_down")) %>% 
  add_header_above(c(" " = 1, "Malaria diagnosis" = 3, "Insurance status" = 3, "Malaria diagnosis" = 3, "Insurance status" = 3), bold = T) %>% 
  add_header_above(c(" " = 1, "Int Patient" = 6, "Out Patient" = 6), bold = T) %>%
  footnote(general = c("All analysis Mean (SD) and Wilcoxon rank sum test")) %>%
   row_spec(0, bold = T, hline_after = T)
```


```{r }
df_merge2 %>% 
  select(-stat_0_1) %>% 
  kable(booktabs = T, format = "latex", linesep = "",
        caption = "Health expenditure description, 2007 survey focused on malaria , expressed in US\\$ 2020 rates.",
        col.names = c("Variable", "Int Patient (n = 149)", "Out Patient (n = 150)"),
        align = c("l", "r", "r"), format.args = list(big.mark = ' ')) %>%
  kable_styling(latex_options = c("HOLD_position")) %>%
  footnote(general = c("All analysis Mean (SD) and Wilcoxon rank sum test")) %>%
  row_spec(0, bold = T, hline_after = T)
```

