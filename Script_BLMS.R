
rm(list = ls())

library(readxl)
library(dplyr)
library(tidyr)
library(gtsummary)
library(knitr)
library(rmarkdown)

#HOOPE study  (2020 survey)

hoope <- read_excel("data/hoope.xlsx") %>% 
  mutate_at(vars(OOP, THE, `Direct medical`, `Direct non medical`),
            ~(. / 500))
#View(hoope)

hoope2 <-
  hoope %>% select(OOP, THE, `Direct medical`, `Direct non medical`,
                   moredeas, typ_pat, assure, malaria)

hoope2_int <-
  hoope %>%
  filter(typ_pat == "inpatient") %>% 
  select(OOP, THE, `Direct medical`, `Direct non medical`,
                   moredeas, typ_pat, assure, malaria)

hoope2_out <-
  hoope %>% 
  filter(typ_pat == "Outpatient") %>% 
  select(OOP, THE, `Direct medical`, `Direct non medical`,
                   moredeas, typ_pat, assure, malaria)
##------------------------------------------------------------------------------
tab1_01 <- 
  hoope2 %>%
  select(1:4) %>% 
  tbl_summary(statistic = list(all_continuous() ~ "{mean} ({sd})",
                               all_categorical() ~ "{n} / {N} ({p}%)"))


tab1_02 <- 
  tbl_summary(
    hoope2_int %>% select(1:4, malaria),
    by = malaria, # split table by group
    statistic = list(all_continuous() ~ "{mean} ({sd})",
                     all_categorical() ~ "{n} / {N} ({p}%)"),
    missing = "no" # don't list missing data separately
  ) %>%
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels()


tab1_03 <- 
  tbl_summary(
    hoope2_out %>% select(1:4, malaria),
    by = malaria, # split table by group
    statistic = list(all_continuous() ~ "{mean} ({sd})",
                     all_categorical() ~ "{n} / {N} ({p}%)"),
    missing = "no" # don't list missing data separately
  ) %>%
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels()



tab1_04 <- 
  tbl_summary(
    hoope2_int %>% select(1:4, assure),
    by = assure, # split table by group
    statistic = list(all_continuous() ~ "{mean} ({sd})",
                     all_categorical() ~ "{n} / {N} ({p}%)"),
    missing = "no" # don't list missing data separately
  ) %>%
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels() 

tab1_05 <- 
  tbl_summary(
    hoope2_out %>% select(1:4, assure),
    by = assure, # split table by group
    statistic = list(all_continuous() ~ "{mean} ({sd})",
                     all_categorical() ~ "{n} / {N} ({p}%)"),
    missing = "no" # don't list missing data separately
  ) %>%
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels()

tab_merge1 <-
  tbl_merge(tbls = list(tab1_01, tab1_02, tab1_04, tab1_03, tab1_05))


h_merge1 <- tab_merge1[1]

df_merge1 <- as.data.frame(h_merge1[['table_body']]) %>% 
  select(Variable = label, stat_0_1, stat_1_2, stat_2_2, p.value_2, stat_1_3, stat_2_3, p.value_3,
         stat_1_4, stat_2_4, p.value_4, stat_1_5, stat_2_5, p.value_5) %>%
  mutate_at(vars(contains("p.value")), list(~ifelse(. == "NA", NA, .))) %>% 
  mutate_at(vars(contains("p.value")), list(~ifelse(. <= 0.001, "< 0.001", round(., 2)))) %>% 
  mutate(Variable = case_when(Variable == "OOP" ~ "OOPHE", TRUE ~ Variable)) %>% 
  arrange(factor(Variable, levels = c("OOPHE", "Direct medical", "Direct non medical", "THE")))


# IPTI study  (2007 survey) ----------------------------------------------------

ipti <- 
  read_excel("data/ipticostdescriptiondata.xlsx") %>% 
  mutate_at(vars(OOP, THE, `Direct medical`, `Direct non medical`),
            ~(. / 500))
#View(ipti)

ipti2 <-
  ipti %>% select(OOP, THE, `Direct medical`, `Direct non medical`,
                  typ_pat, diagnosticsecondaire)


## Table 2 ---------------------------------------------------------------------
tab2_01 <- 
  ipti2 %>%
  select(1:4) %>% 
  tbl_summary(statistic = list(all_continuous() ~ "{mean} ({sd})",
                               all_categorical() ~ "{n} / {N} ({p}%)"))


tab2_02 <- 
  tbl_summary(
    ipti2 %>% select(1:4, typ_pat),
    by = typ_pat, # split table by group
    statistic = list(all_continuous() ~ "{mean} ({sd})",
                     all_categorical() ~ "{n} / {N} ({p}%)"),
    missing = "no" # don't list missing data separately
  ) %>%
  add_p() %>% # test for a difference between groups
  modify_header(label = "**Variable**") %>% # update the column header
  bold_labels() 


tab_merge2 <-
  tbl_merge(tbls = list(tab2_01, tab2_02))


h_merge2 <- tab_merge2[1]

df_merge2 <-
  as.data.frame(h_merge2[['table_body']]) %>% 
  select(Variable = label, stat_0_1, stat_1_2, stat_2_2) %>%
  mutate(Variable = case_when(Variable == "OOP" ~ "OOPHE", TRUE ~ Variable)) %>% 
  arrange(factor(Variable, levels = c("OOPHE", "Direct medical", "Direct non medical", "THE")))



reportfolder <- "reports/"
filereportRmd <- "report_BLMS.Rmd"
output_DIR <- paste0(reportfolder, "/Report_analyse_blell_", format(Sys.Date(), "%Y-%m-%d"))
rmarkdown::render(filereportRmd, output_file = output_DIR, quiet = TRUE)
